import React from 'react';
import {Form} from 'react-formio';
import formData from './formData.json';
import 'formiojs/dist/formio.builder.min.css'
import 'bootstrap/dist/css/bootstrap.min.css'

class MySimpleForm extends React.Component {

  handleSubmit(data) {
    console.log(data);
  }

  render() {
    return (
      <div style={{maxWidth: '400px', margin: '50px'}}>
        <Form form={formData} onSubmit={this.handleSubmit}/>
      </div>
    )
  }
}

export default MySimpleForm